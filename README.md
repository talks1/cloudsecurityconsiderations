# Security Review

## Problem:

We want to `mitigate`  
- the financial and information risk from our infrastructure
  - GCloud malicious use $14k
  - Azure malicious use $15k

We want to `retain`
- our ability to be flexible and quickly create new applications.

## Top Risk Exposure

The top risks from our infrastructure include:

- Direct Costs
  - malicious VM use which results in large compute cost charges
  - malicious Network use which results in large egress charges

## Additional Risks
- Customers choice
  - Customers choose not to use us for delivery due to security concerns
  - IT departments resist our solutions due to security concerns

- Sensitive Data
  - exposure of sensitive data

- Malicous removal of resources

## Concerns with cloud usage

- VMs created using cloud provider 
  - defaults which enable global access
  - with username/password access rather than private keys
- Storage of credentials on cloud VMs
- Exposing application service (ports) which are not secure.
- User Accounts dont require 2 factor auth
- Cloud directories not help us make good passwords
- Compartmentalization is not provided by default
- Monitoring takes extra work to set up
- We dont have any anomaly dection

- No way to limit liability

- Are we using good practice to lock down 'Compute API' invocations?

## Barrier to migitaging risks

- Establishment of least privilege principal is time consuming
- Security is generally not our primary concern as the applications we develop are not enterprise systems.
- No ability to cap usage limits with cloud resources
- Defaults provided by cloud providers are not necessarily secure

- No BST code to pay for security investment, unable to save time.

## Context

Our security measure need to be compatible with the type of work we do.   In security the cure cant be worse than the disease.

What type of work are we trying to cater for into the future?
- short engagements 0-3 months
- 1-2 people on the project
- dev environments in the cloud
- ability to use latest/common use software (rather then using secure but outdated solutions)

## Prioritized Options As of 2020-04-15 

The integration solutions team discussed the optiosn in this section on 2020-04-15.

The following items have been identified as worthy of further investigation and investment.

- Better identity management control  (underway)
    - Continue to progress
    - Include in a proposal to management
    - Go through OS Login 

- Shut things down which arent really needed (Compute, Networks)
    - Ideally part of each project but some repetitive effort.
    - Continue to progress

- More alerting  set up to limit the damage of break ins.
    - Have session to cover off existing alert and what is possible
    - Continue to progress this

- Serverless
    - Good benefit from a security perspective
    - Doesnt work on premise
    - Try getting more experience with this approach
- Force a re-creation every period X
    - Learning curve
    - May be a lot of work
    - Which has additional benefits (retain working knowlege) and costs (changing something not broken)   
      - How frequently do we review
    - Get more experience with this approach

The folowing items have been considered but are not considered worthy of further investigation and investment.

- Deploy VMs behind a gateway/bastion or VPN which can be a central goverance control point where we increase our spend
    - Somewhat make working in the cloud more difficult
    - Learning curve on how to do this
    - Working with customer this way is probably how we work with customers


- Use K8S/App Services (Firebase, AppEngine) as a deployment container rather then use VMs
    - Limits our flexibility
    - Learning curve to get up to speed
    - Provider specific technologies which are likely to evolve considerably
    - Difference between working locally and working in the cloud

- Deploy application on top of K8S
    - Somewhat limits flexibility
    - Additional complexity 
    - Security is not a primary use case for K8S


- Create VM templates which have stricter defaults 
  - Time consuming and how to we get people to use these
   - Use different distributions as a default rather than default Ubuntu
   - Also could use windows servers.

- Stop using cloud

## Knowns

- Using cloud service like gitlab runs on unknown servers which makes it difficult to lock down target VMs 
  - It may be appropriate to run our own gitlab runner for our projects to reduce the security risk

- AWS limits the number of firewall rules which means we have to leave those VMs open to the world, Google also large number of VMs which means they can be somewhat locked down but anyone malicious user from a google vm will have access

## What options do we want to invest in

## How should we invest in them

1. Spend some portion of every project working on them
2. Spend time on these during quiet periods
3. Make a proposal for funding to spend time on these item options
  - WHats the goal

