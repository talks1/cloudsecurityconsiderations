'use strict';

async function main(
  name = 'pjt-vm' // VM name of your choice
) {
  const Compute = require('@google-cloud/compute');

  async function deleteVM() {
    const compute = new Compute();
    const zone = compute.zone('australia-southeast1-b');
    const vm = zone.vm(name);
    const [operation] = await vm.delete();
    console.log(`VM deleting`);
    await operation.promise();
    console.log(`VM deleted!`);
  }
  deleteVM();
}

main(...process.argv.slice(2));