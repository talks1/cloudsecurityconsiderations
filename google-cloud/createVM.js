  // Imports the Google Cloud client library
  const Compute = require('@google-cloud/compute');

  // Creates a client
  const compute = new Compute();

  async function quickstart() {
    // Create a new VM using the latest OS image of your choice.
    const zone = compute.zone('australia-southeast1-b');

    // TODO(developer): choose a name for the VM
    const vmName = 'pjt-vm';

    // Start the VM create task
    const [vm, operation] = await zone.createVM(vmName, {os: 'ubuntu'});
    console.log(vm);

    // `operation` lets you check the status of long-running tasks.
    await operation.promise();

    // Complete!
    console.log('Virtual machine created!');
  }
  quickstart();